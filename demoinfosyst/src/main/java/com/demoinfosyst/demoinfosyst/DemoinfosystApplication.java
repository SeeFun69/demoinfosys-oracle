package com.demoinfosyst.demoinfosyst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoinfosystApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoinfosystApplication.class, args);
	}

}
