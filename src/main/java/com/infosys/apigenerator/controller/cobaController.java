package com.infosys.apigenerator.controller;

import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class cobaController {
    @GetMapping("/v1/messages")
    public String welcome(){
        return "Welcome to Springboot Rest API";
    }
    @PostMapping("/v1/halo/{name}")
    @ResponseBody
    public static Map<String, Object> defaultController(@PathVariable String name){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("message", "hello" + name);
        return map;
    }

    @GetMapping("/v1/halo/{name}")
    @ResponseBody
    public static Map<String, Object> defaultCOn(@PathVariable String name){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("message", "hello" + name);
        return map;
    }
}
