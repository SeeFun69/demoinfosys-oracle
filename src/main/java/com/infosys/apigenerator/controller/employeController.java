package com.infosys.apigenerator.controller;

import com.infosys.apigenerator.entity.employee;
import com.infosys.apigenerator.service.employeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
//@Controller
public class employeController {
    @Autowired
    private employeService service;

    @GetMapping("/employe")
    public Iterable<employee> employeList(){
        return service.getAll();
    }
//    @GetMapping("/employee")
//    public String coba(Model model){
//        List<employee> employeeList = service.getAll();
//        model.addAttribute("employeeList",employeeList);
//        return "employee";
//    }

//    @GetMapping("/users")
//    public String showUserList(Model model){
//        List<employee> listUsers = service.listAll();
//        model.addAttribute("listUsers",listUsers);
//
//        return "users";
//    }
}
