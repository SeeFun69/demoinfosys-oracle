package com.infosys.apigenerator.entity;

import javax.persistence.*;

@Entity
@Table(name = "EMPLOYEES")
public class employee {
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer employeeId;

    @Column(length = 45, nullable = false, name = "first_name")
    private String firstName;

    @Column(length = 45, nullable = false,name = "last_name")
    private String lastName;

    @Column(nullable = false, unique = true, length = 45)
    private String email;

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
