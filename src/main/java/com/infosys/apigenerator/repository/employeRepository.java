package com.infosys.apigenerator.repository;

import com.infosys.apigenerator.entity.employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface employeRepository extends JpaRepository<employee, Integer> {
//    List<employee> getAll();
}
