package com.infosys.apigenerator.service;

import com.infosys.apigenerator.entity.employee;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.infosys.apigenerator.repository.employeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class employeService{
    @Autowired
    private employeRepository repo;

    public List<employee> getAll() {
        return repo.findAll();
    }

//    @Override
//    public <S extends employee> S save(S entity) {
//        return null;
//    }
//
//    @Override
//    public <S extends employee> Iterable<S> saveAll(Iterable<S> entities) {
//        return null;
//    }
//
//    @Override
//    public Optional<employee> findById(Integer integer) {
//        return Optional.empty();
//    }
//
//    @Override
//    public boolean existsById(Integer integer) {
//        return false;
//    }
//
//    @Override
//    public Iterable<employee> findAll() {
//        return this.repo.findAll();
//    }
//
//    @Override
//    public Iterable<employee> findAllById(Iterable<Integer> integers) {
//        return null;
//    }
//
//    @Override
//    public long count() {
//        return 0;
//    }
//
//    @Override
//    public void deleteById(Integer integer) {
//
//    }
//
//    @Override
//    public void delete(employee entity) {
//
//    }
//
//    @Override
//    public void deleteAllById(Iterable<? extends Integer> integers) {
//
//    }
//
//    @Override
//    public void deleteAll(Iterable<? extends employee> entities) {
//
//    }
//
//    @Override
//    public void deleteAll() {
//
//    }
}
